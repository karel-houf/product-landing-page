window.onscroll = () => {
    stickyNavbar();
};

const navbar = document.getElementById("nav-bar");
const navbarOffset = navbar.offsetTop;

const stickyNavbar = () => {
    if (window.pageYOffset >= navbarOffset) {
        navbar.classList.add("sticky-nav");
    } else {
        navbar.classList.remove("sticky-nav");
    }
}